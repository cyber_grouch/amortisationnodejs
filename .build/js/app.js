'use strict';



requirejs.config({
    baseUrl: 'components',
    paths: {
        jquery: 'jquery/dist/jquery',
        bootstrap: 'bootstrap/dist/js/bootstrap',
        validator: 'bootstrap-validator/dist/validator'
    },
    shim: {
        "bootstrap": {
            deps: ["jquery"]
        },
        "validator": {
            deps: ["jquery", 'bootstrap']
        }
    }
});


require(['jquery', 'bootstrap', 'validator'], function () {

    var app = {
        initialize: function () {
            $(document).ready(function() {
                $('#modalForm').validator();
            });
        }
    };

    app.initialize();

});


require(['jquery'], function() {

    $(document).ready(function() {
        $('#calculate').on('click', function() {
            if ($('.form-group.has-error').size() == 0) {
                $('#modalForm').submit();
            }
        });

    })

});
