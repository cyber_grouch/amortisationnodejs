'use strict';

var amortisation = require('./amortisation');

module.exports = function LoanModel(amount, interest, years) {

    var amortisationModel = amortisation.AmortisationModel(amount, interest, years);

    return {
        amount: amount,
        interest: interest,
        years: years,
        scheduleItems: amortisationModel.scheduleItems
    };
};