'use strict';


module.exports.ScheduleItem = function(paymentNumber,
                                       curMonthlyPaymentAmount,
                                       curMonthlyInterest,
                                       curBalance,
                                       totalPayments,
                                       totalInterestPaid) {

    function float2int(value) {
        return value | 0;
    };

    function display(value) {
        return 1.0 * float2int(value) / 100;
    };

    return {
        paymentNumber: paymentNumber,
        curMonthlyPaymentAmount: curMonthlyPaymentAmount,
        curMonthlyInterest: curMonthlyInterest,
        curBalance: curBalance,
        totalPayments: totalPayments,
        totalInterestPaid: totalInterestPaid,

        displayPaymentNumber: paymentNumber,
        displayCurMonthlyPaymentAmount: display(curMonthlyPaymentAmount),
        displayCurMonthlyInterest: display(curMonthlyInterest),
        displayCurBalance: display(curBalance),
        displayTotalPayments: display(totalPayments),
        displayTotalInterestPaid: display(totalInterestPaid)

    };
};