'use strict';

var schedule = require('./scheduleItem');

module.exports.AmortisationModel = function (amountParam, interestParam, yearsParam) {

    function float2int(value) {
        return value | 0;
    };

    function inverse(value) {
        return Math.pow(value, -1);
    };

    var amount = float2int(parseFloat(amountParam) * 100),
        interest = parseFloat(interestParam) / 100,
        years = parseInt(yearsParam),
        months = years * 12,
        monthlyInterest = interest / 12;


    var tmp = inverse(1 + monthlyInterest);
    tmp = Math.pow(tmp, months);
    tmp = inverse(1 - tmp);

    var monthlyPaymentAmount = float2int(monthlyInterest * amount * tmp);

    var balance = amount,
        paymentNumber = 1,
        curScheduleItem = null,
        scheduleItems = [],
        totalPayments = 0,
        totalInterestPaid = 0;

    while (balance >= 100) {

        var curMonthlyInterest = balance * monthlyInterest,
            curPayoffAmount = balance * curMonthlyInterest,
            curMonthlyPaymentAmount = Math.min(monthlyPaymentAmount, curPayoffAmount);

        if ((curMonthlyPaymentAmount == 0)
            || (curMonthlyPaymentAmount == curMonthlyInterest)) {
            curMonthlyPaymentAmount = curPayoffAmount;
        }

        var curMonthlyPrincipalPaid = curMonthlyPaymentAmount - curMonthlyInterest;

        var curBalance = balance - curMonthlyPrincipalPaid;

        if (curBalance < 0 || (curMonthlyPrincipalPaid == 0)) {
            curMonthlyInterest = curMonthlyInterest + curBalance;
            curBalance = 0;
        }

        if (curScheduleItem) {
            totalPayments = curScheduleItem.totalPayments;
            totalInterestPaid = curScheduleItem.totalInterestPaid;
        }
        totalPayments = totalPayments + curMonthlyPaymentAmount;
        totalInterestPaid = totalInterestPaid + curMonthlyInterest;

        curScheduleItem = schedule.ScheduleItem(paymentNumber,
                                                curMonthlyPaymentAmount,
                                                curMonthlyInterest,
                                                curBalance,
                                                totalPayments,
                                                totalInterestPaid);
        scheduleItems.push(curScheduleItem);

        balance = curScheduleItem.curBalance;

        paymentNumber = paymentNumber + 1;

    }

    return {
        scheduleItems: scheduleItems
    };
};