'use strict';

module.exports = function () {
    return function (req, res, next) {
        console.log("HEADERS: " + JSON.stringify(req.headers));

        console.log("QUERY: " + JSON.stringify(req.query));

        console.log("PAYLOAD: " + JSON.stringify(req.body));

        next();
    }
};