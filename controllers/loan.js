'use strict';


var LoanModel = require('../models/loan');


module.exports = function (app) {

    app.post('/loan', function (req, res) {
        var model = new LoanModel(req.body.amount, req.body.rate, req.body.years);
        res.render('loan', model);
    });

};
